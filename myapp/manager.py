from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations=True

    def create_user(self,phone_number,password=None,**extra_fields):
        if not phone_number:
            raise ValueError('phone number is required')

        user=self.model(phone_number=phone_number,**extra_fields)
        user.set_password(password)
        user.save()
        return user
   
    def create_superuser(self,phone_number,password=None,**extra_fields):
        user = self.create_user(
            phone_number,
            password=password,
            **extra_fields,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user